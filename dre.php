<!DOCTYPE html>
<html>
<head>
<meta charset=utf8 />
<title>Balanço Patrimonial & DRE</title>
</head>
<body>

<?php

//include("check.php");
include("connect.php");

if (!$db) {
    die('Could not connect: ' . mysqli_error($db));
}

$sql="SELECT * FROM CONTAS WHERE nat IN (80, 81) ORDER BY `nat`";
$result = mysqli_query($db,$sql);

echo "<table border=1><tr><th>Demonstrativo de Resultado</th><th>Valor</th></tr>";

$dre=0;

while($row = mysqli_fetch_array($result)) {
    echo "<tr><td>" . $row['nam'] . "</td><td>";

    $dre+=calc_credit($db, $row['id']);
    echo "</td></tr>";
}

echo "<tr><td>Resultado Líquido</td><td>$dre</td></tr>";
echo "</table>";


function calc_credit($db, $data) {
    $sql2="SELECT * FROM RAZAO WHERE ct=".$data." ORDER BY `nt`";
    $query2=mysqli_query($db,$sql2);
    $sld=0;
    while($r = mysqli_fetch_array($query2)) {
        if ($r['nt'] == 'C') {
            $sld+=$r['vl'];
        }
        if ($r['nt'] == 'D') {
            $sld-=$r['vl'];
        }
    }
    if ($sld > 0) {
        echo "$sld";
    } else {
        $sld*=-1;
        echo "($sld)";
        $sld*=-1;
    }
    return $sld;
}

function calc_debt($db, $data) {
    $sql2="SELECT * FROM RAZAO WHERE ct=".$data." ORDER BY `nt`";
    $query2=mysqli_query($db,$sql2);
    $sld=0;
    while($r = mysqli_fetch_array($query2)) {
        if ($r['nt'] == 'D') {
            $sld+=$r['vl'];
        }
        if ($r['nt'] == 'C') {
            $sld-=$r['vl'];
        }
    }
    if ($sld > 0) {
        echo "$sld";
    } else {
        $sld*=-1;
        echo "($sld)";
        $sld*=-1;
    }
    return $sld;
}
?>

<br/>
<br/>







<?php

$acirc ="SELECT * FROM CONTAS WHERE nat IN (0,1,2,3,4) ORDER BY `nat`";
$racirc = mysqli_query($db,$acirc);
$ancirc ="SELECT * FROM CONTAS WHERE nat IN (10,11,12,13,14,15,16,17) ORDER BY `nat`";
$rancirc = mysqli_query($db,$ancirc);

$pcirc ="SELECT * FROM CONTAS WHERE nat IN (40, 41) ORDER BY `nat`";
$rpcirc = mysqli_query($db,$pcirc);
$pncirc ="SELECT * FROM CONTAS WHERE nat IN (42, 43) ORDER BY `nat`";
$rpncirc = mysqli_query($db,$pncirc);

$pl ="SELECT * FROM CONTAS WHERE nat IN (95, 96) ORDER BY `nat`";
$rpl = mysqli_query($db,$pl);













echo "<table border=1><caption>Balanço Patrimonial</caption><tr><td>"; // MASTER TABLE
echo "<table border=1><tr><th>Ativo</th><th/></tr>";

$ativo=0;

echo "<tr><th>Ativo Circulante</th><th/></tr>";
while($row = mysqli_fetch_array($racirc)) {
    echo "<tr><td>" . $row['nam'] . "</td><td>";
    $ativo+=calc_debt($db, $row['id']);
    echo "</td></tr>";
}

echo "<tr><th>Ativo Não Circulante</th><th/></tr>";
while($row = mysqli_fetch_array($rancirc)) {
    echo "<tr><td>" . $row['nam'] . "</td><td>";
    $ativo+=calc_debt($db, $row['id']);
    echo "</td></tr>";
}

echo "<tr><th>Ativo Total</th><th>$ativo</th></tr>";
echo "</table>";





echo "</td><td>"; // MASTER TABLE
echo "<table border=1><tr><th>Passivo</th><th/></tr>";

$lado=0;

echo "<tr><th>Passivo Circulante</th><th/></tr>";
while($row = mysqli_fetch_array($rpcirc)) {
    echo "<tr><td>" . $row['nam'] . "</td><td>";
    $lado+=calc_credit($db, $row['id']);
    echo "</td></tr>";
}

echo "<tr><th>Passivo Não Circulante</th><th/></tr>";
while($row = mysqli_fetch_array($rnpcirc)) {
    echo "<tr><td>" . $row['nam'] . "</td><td>";
    $lado+=calc_credit($db, $row['id']);
    echo "</td></tr>";
}

echo "<tr><th>Patrimônio Líquido</th><th/></tr>";
while($row = mysqli_fetch_array($rpl)) {
    echo "<tr><td>" . $row['nam'] . "</td><td>";
    $lado+=calc_credit($db, $row['id']);
    echo "</td></tr>";
}
echo "<tr><td>Lucro do Exercício</td><td>$dre</td></tr>";
$lado+=$dre;

echo "<tr><th>Passivo + PL</th><th>$lado</th></tr>";
echo "</table>";

echo "</td></tr></table>"; // MASTER TABLE

?>



<a href=index>← Voltar</a>

<!--table border=1>
<tr><td><table border=1><tr><td>dentro</td><td>ladentro</td></tr></table>fore</td><td>lado</td></tr>
</table-->
</body>
</html>
<?php mysqli_close($db); ?>
