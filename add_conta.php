<?php

/*
include("check.php");	
if ($access_lvl < 80 && $access_lvl != 20) {
    header("Location: failure.php"); // Unauthorized if not: Comissão ou Sobreescrita
}
*/

include("connect.php");

if (($_SERVER["REQUEST_METHOD"] == "POST")) {
        $err=0;

        if (empty($_POST["natureza"]) || empty($_POST["name"])) {
            $err=1;
        }

        $name=test_input($_POST["name"]);
        $nat=test_input($_POST["natureza"]);

        if ($nat == 1227) {
            $nat = 0;
        }

        //echo "$nat<br>";

        if (!$err) {
            $sql = "INSERT INTO CONTAS (nat,nam)
            VALUES ($nat, '$name')";

            // Did everything went good?
            if ($db->query($sql) === TRUE) {
                #$last_id = $db->insert_id;
                $err=0;
            } else {
                $err=2;
                echo "<br>Erro fatal ao registrar conta: " . $db->error;
            }
        }
        if (!$err) {
            echo "<u>Sucesso, por favor recarregue a página para verificar o novo plano de contas.</u>";
        } else {
            echo "ERROR HAPPENED: $err ";
        }
        $db->close();
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

?>


<html>
<head>
<meta charset=UTF-8 />
<title>Configuração de Contas</title>
<link rel="stylesheet" type="text/css" href="pres18.css" media="all">
<link rel="icon" type="image/ico" id="favicon" href="/icon.ico" />
<script>
function showPart(str) {
    // Does not work in some old browsers we don't care about.
    var syschapa = new XMLHttpRequest();
    syschapa.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("confirm").innerHTML = this.responseText;
        }
    };
    syschapa.open("GET","list_conta.php?q="+str,true);
    syschapa.send();
}
</script>
</head>

<body>
<div id=main></div>
<!--
<script>
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open("GET","main.php",true);
        xmlhttp.send();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("main").innerHTML = this.responseText;
            }
        };
</script>
-->
<br/>
<form method="post" action="">
<h1>Cadastro de Conta</h1>
Natureza: <!--<input id="id" name="id" class="element number small" type="number" maxlength="9" required/> <br/><br/>-->

<select name="natureza">
<option value=1227>Disponível</option>
<option value=1>Estoque</option>
<option value=2>(-) Estoque</option>
<option value=3>Circulante</option>
<option value=4>(-) Circulante</option>

<option value=0>----------------------</option>

<option value=10>RLP</option>
<option value=11>(-) RLP</option>
<option value=12>Investimento</option>
<option value=13>(-) Investimento</option>
<option value=14>Imobilizado</option>
<option value=15>(-) Imobilizado</option>
<option value=16>Intangível</option>
<option value=17>(-) Intangível</option>

<option value=0>----------------------</option>

<option value=40>Passivo C.</option>
<option value=41>(-) Passivo C.</option>
<option value=42>Passivo NC.</option>
<option value=43>(-) Passivo NC.</option>

<option value=0>----------------------</option>
<option value=80>Receita</option>
<option value=81>Despesa</option>

<option value=95>PL</option>
<option value=96>(-) PL</option>

</select><br/><br/>

Nome da Conta: <input name="name" type="text" maxlength="50" placeholder="Caixa" required /> <br/>

<input type="submit" value="Cadastrar Conta" />
</form>

</body>
</html>
