<?php

//include("check.php");
include("connect.php");

if (!$db) {
    die('Could not connect: ' . mysqli_error($db));
}

if (($_SERVER["REQUEST_METHOD"] == "POST")) {
    $err=0;

    if (empty($_POST["debt"]) || empty($_POST["cred"]) || empty($_POST["ht"]) || empty($_POST["vl"])) {
        $err=1;
        echo "Uma das entradas é inválida.";
    }

    if (!$err) {
        $debt=test_input($_POST["debt"]);
        $cred=test_input($_POST["cred"]);
        $vl=test_input($_POST["vl"]);
        $ht=test_input($_POST["ht"]);

        $sql1 = "INSERT INTO DIARIO (debt, cred, val, ht)
        VALUES ($debt, $cred, $vl, '$ht');";
        $sql2 = "INSERT INTO RAZAO (ct, nt, vl)
        VALUES ($debt, 'D', $vl);";
        $sql3 = "INSERT INTO RAZAO (ct, nt, vl)
        VALUES ($cred, 'C', $vl);";

        // Did everything went good?
        if (!$err && !($db->query($sql1) === TRUE)) { $err=4; }
        if (!$err && !($db->query($sql2) === TRUE)) { $err=6; }
        if (!$err && !($db->query($sql3) === TRUE)) { $err=8; }

        if ($err) {
            echo "<br>Erro fatal ao registrar conta: " . $db->error . " (código $err)";
        } else {
            header("Location: diario.php#new");
        }

    }
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
?>

<!DOCTYPE html>
<html>
<head>
<meta charset=utf8 />
<title>Livro Diário - Inserção</title>
</head>
<body>
<h2>Adicionar Entrada</h2>
<form method="post" action="">
<table border=1>
<tr><td>Débito</td><td>
<select name="debt" required />

</td></tr>
<tr><td>Crédito</td><td>
<select name="cred" required />
</td></tr>
<tr><td>Histórico</td><td> <input name="ht" type="text" required /> </td></tr>
</table>
<input type="submit" value="Registrar" />
</form>
</body>
</html>
<?php mysqli_close($db); ?>
