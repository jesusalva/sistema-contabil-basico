/* This script creates user and database for installation.
    Please edit connect.php to change stuff.
    Protip: install.php will read connect.php to do stuff right,
            but database and user must already exist */

CREATE DATABASE icont;
CREATE USER 'cont'@'localhost' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON `icont`.* to 'cont'@'localhost';
FLUSH PRIVILEGES;

CREATE TABLE icont.DIARIO (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    debt INT(6) NOT NULL,
    cred INT(5) NOT NULL,
    val FLOAT NOT NULL,
    ht VARCHAR(480) NOT NULL,
    dt TIMESTAMP UNIQUE KEY
    );

CREATE TABLE icont.CONTAS (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nat INT(3) NOT NULL,
    nam VARCHAR(50) NOT NULL
    );

CREATE TABLE icont.RAZAO (
    id INT(9) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    ct INT(6) NOT NULL,
    nt ENUM('D', 'C') NOT NULL,
    vl FLOAT NOT NULL,
    dt TIMESTAMP
    );
