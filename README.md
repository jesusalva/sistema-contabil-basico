# Installation

To install on a Linux, use `init.sh`

If you don't use Linux, you can browse to `install.php`, but database and user
must already exist, and be defined in connect.php

# Instalação

Para instalar em um Linux, use `init.sh`

Se não, navegue para `install.php`, porém o banco de dados e usuário já devem
existir e estar definidos em connect.php
## Observação
software criado como prova de conceito para projeto
universitário.