<html>
<head>
<meta charset=UTF-8 />
<title>Installation</title>
<link rel="stylesheet" type="text/css" href="view.css" media="all">
<link rel="icon" type="image/ico" id="favicon" href="/icon.ico" />
</head>
<body>
	<img id="top" src="top.png" alt="">
	<div id="form_container">
<success>
<?php
    // Connects to database
    include("connect.php");
    
    // Check connection
    if ($db->connect_error) {
        //die("Connection failed: " . $db->connect_error);
        $err = 2;
        echo "<br>Conexão Falhou: " . $db->connect_error;
        $msg = "O servidor falhou em se conectar ao banco de dados principal: " . $db->connect_error;
    }
    echo "<br>Conexão estabelecida!";


    // XXX TODO One-time-use-only: Create table
    // sql to create table TODO FIXME DEMO ONLY FIXME TODO
    // DIARIO: Livro Diário
    // id → valor automático, para consistência
    // debt → Débito (ID)
    // cred → Crédito (ID)
    // val → Valor
    // dt → data, para consistência
    $sql = "CREATE TABLE DIARIO (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    debt INT(6) NOT NULL,
    cred INT(5) NOT NULL,
    val FLOAT NOT NULL,
    ht VARCHAR(480) NOT NULL,
    dt TIMESTAMP UNIQUE KEY
    )";

    if ($db->query($sql) === TRUE) {
        echo "<br>Tabela Diário criada.<br>";
    } else {
        echo "<br>Error creating table: " . $db->error;
    }



    // XXX TODO One-time-use-only: Create table
    // sql to create table TODO FIXME DEMO ONLY FIXME TODO
    // CONTAS: Plano de Contas
    // id → ID da conta
    // nat → Natureza da conta (classe da estrutura conceitual)
    // nam → Nome Humano da Conta
    $sql = "CREATE TABLE CONTAS (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nat INT(3) NOT NULL,
    nam VARCHAR(50) NOT NULL
    )";

    if ($db->query($sql) === TRUE) {
        echo "<br>Tabela do Plano de Contas criada.<br>";
    } else {
        echo "<br>Error creating table: " . $db->error;
    }

    // Naturezas
    /*
    0 - Ativo Circulante Disponível
    1 - Ativo Circulante Estoques
    2 - (-) ACE
    3 - Ativo Circulante Diverso
    4 - (-) ACD

    10 - RLP
    11 - (-) RLP
    12 - INV
    13 - (-) INV
    14 - IMB
    15 - (-) IMB
    16 - INT
    17 - (-) INT

    40 - PC
    41 - (-) PC
    42 - PNC
    43 - (-) PNC

    80 - RC
    81 - DP

    95 - PL
    96 - (-) PL
    */



    // XXX TODO One-time-use-only: Create table
    // sql to create table TODO FIXME DEMO ONLY FIXME TODO
    // RAZAO → Livro Razão
    // id → Controle Interno.
    // ct → Conta
    // nt → Natureza (Débito 0/Crédito 1)
    // vl → Valor
    // dt → data
    $sql = "CREATE TABLE RAZAO (
    id INT(9) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    ct INT(6) NOT NULL,
    nt ENUM('D', 'C') NOT NULL,
    vl FLOAT NOT NULL,
    dt TIMESTAMP
    )";

    if ($db->query($sql) === TRUE) {
        echo "<br>Tabela do Livro Razão criada.<br>";
    } else {
        echo "<br>Error creating table: " . $db->error;
    }



    /*
    // XXX TODO One-time-use-only: Create table
    // sql to create table TODO FIXME DEMO ONLY FIXME TODO
    // Part 1801: Tabela de Partidos
    // id → Correspondente VT para o painel de apuração
    // name → Nome da chapa (até 40 caracteres)
    // note → Um tweet (120 char) que será exibido
    // main → Nome e matrícula do Diretor Geral da chapa (120 char)
    // dt → data de matrícula do partido (apenas caso seja necessário utilizar o phpMyAdmin)
    // Esta tabela apenas possui uma entrada.
    $sql = "CREATE TABLE Part1801 (
    id INT(2) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(40) NOT NULL,
    note VARCHAR(120) NOT NULL,
    main VARCHAR(120) NOT NULL UNIQUE KEY,
    dt TIMESTAMP
    )";

    if ($db->query($sql) === TRUE) {
        echo "<br>Tabela de Partidos criada.<br>";
    } else {
        echo "<br>Error creating table: " . $db->error;
    }





    // XXX TODO One-time-use-only: Create table
    // sql to create table TODO FIXME DEMO ONLY FIXME TODO
    // Log 1801: Tabela de Registro (Administrativo)
    // ip → Consistência (IP de origem)
    // ac → Registro da ação
    // who → Quem supostamenye realizou a ação
    // dt → data, para consistência
    $sql = "CREATE TABLE Log1801 (
    ip VARCHAR(40) NOT NULL,
    ac VARCHAR(4000) NOT NULL,
    who INT(9) NOT NULL,
    dt TIMESTAMP
    )";

    if ($db->query($sql) === TRUE) {
        echo "<br>Tabela de Registros criada.<br>";
    } else {
        echo "<br>Error creating table: " . $db->error;
    }





    // XXX TODO One-time-use-only: Create table
    // sql to create table TODO FIXME DEMO ONLY FIXME TODO
    // Auth 1801: Tabela de Autoriza (Determina quais códigos Autoriza são aceitos)
    // ip → Quem criou o registro Autoriza
    // auth → Código Autoriza
    // who → Quem supostamente é o responsável pelo Autoriza
    // dt → data de modificação (consistência)
    $sql = "CREATE TABLE Auth1801 (
    ip VARCHAR(40) NOT NULL,
    auth INT(9) NOT NULL,
    who VARCHAR(80) NOT NULL,
    dt TIMESTAMP
    )";

    if ($db->query($sql) === TRUE) {
        echo "<br>Tabela de Autenticação criada.<br>";
    } else {
        echo "<br>Error creating table: " . $db->error;
    }





    // TODO Consistência Inicial
    
    $sql = "INSERT INTO Ctrl1801 (st, sg)
    VALUES (0, 170037215)";
    if ($db->query($sql) === TRUE) {
        echo "<br>Operação de Sobreescrita realizada, controle informado!<br>";
    }

    // TODO Consistência Inicial
    $sql = "INSERT INTO Part1801 (name,note,main)
    VALUES ('Branco', 'Para votar em branco.', 'Nulo')";
    if ($db->query($sql) === TRUE) {
        echo "<br>Partido (Branco) criado.<br>";
    }
    
    */
?>


<h2>SETUP</h2>

<br /><br/>
O Programa de inicialização do scb<br/><br/><br/>
<b>ACESSO RESTRITO</b>


</success>

</div>


</body>
</html>
<?php mysqli_close($db); ?>
